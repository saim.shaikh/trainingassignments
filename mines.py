import numpy as np
import matplotlib as plt
import pandas as pd
import graphviz
from matplotlib import pyplot
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.decomposition import PCA
from sklearn import preprocessing
from sklearn import tree
from sklearn.metrics import recall_score
from sklearn.metrics import precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.metrics import roc_curve
from sklearn.model_selection import GridSearchCV
from sklearn import svm



#Load the Data
#Separate the features and the Labels present in the data
#Separate the data into training and test sets 70:30 split and shuffle the data
#Binarize the labels to 0 and 1
#OutPut : train data,test data, train labels, test labels 
def load_and_preprocess():
	data  = pd.read_csv('sonar.all-data',header = None)
	data = np.array(data)
	x = data[:,:60]
	y = data[:,60]
	train_x,test_x,train_y,test_y = train_test_split(x,y,test_size = 0.30, random_state = 42,shuffle =True)
	lb = preprocessing.LabelBinarizer()
	train_y = lb.fit_transform(train_y)
	test_y = lb.fit_transform(test_y)
	return train_x,test_x,train_y,test_y,x,y


#Perform grid search to find out best parameters for the SVM Classifier
#Parameters for 'C' range between 0.1 and 10 and 'gamma' between 0.01 to 1
#Parameters are searched and best parameters are chosen for 'C' and 'Gamma'
def gridsearchSVM(train_x,train_y):
	param_grid = [ {'C': np.arange(0.1,10,0.1),'gamma':np.arange(0.01,10,0.1)}] 
	# 'gamma':np.arange(0.01,10,0.1)}]
	sv_grid = svm.SVC(kernel='rbf',random_state = 42,gamma = 'scale')
	grid_svm =  GridSearchCV(sv_grid,param_grid,cv = 5,n_jobs = -1)
	grid_svm.fit(train_x,train_y)
	print(grid_svm.best_params_['C'])
	print(grid_svm.best_params_['gamma'])
	return grid_svm.best_params_['C'],grid_svm.best_params_['gamma']

#Input: Train  data with labels
#The SVM , Decision Tree and Random Forest models are fit to the train data 
#Output: Models for Decision Tree, Random Forest , SVM
def models(train_x,train_y,x,y):
	model_dtree = tree.DecisionTreeClassifier( criterion = 'gini',min_samples_split = 12,max_features ='sqrt',max_depth = 4,random_state = 42)
	model_dtree = model_dtree.fit(train_x,train_y)
	# c,gamma = gridsearchSVM(x,y)
	model_svm = svm.SVC(C=9.3, gamma=0.21, kernel = 'rbf',degree = 2, random_state = 42,probability = True)
	model_svm.fit(train_x, train_y) 
	model_rf = RandomForestClassifier(n_estimators=1000, max_depth=3,random_state=42,min_samples_split = 12,max_features ='sqrt')
	model_rf.fit(train_x,train_y)
	return model_dtree,model_svm,model_rf 

#Input: Data with labels
#This function caculates metrics such as precision recall accuracy f1 score and area under the curve
#The results are printed for every model
def metrics(name,model,x,y,train_x,test_x,train_y,test_y):
	prob = model.predict_proba(test_x)[:,1]
	pred = model.predict(test_x)
	train_acc = model.score(train_x,train_y)
	val_acc = cross_val_score(model, x, y, cv=5).mean()
	test_acc  =  model.score(test_x,test_y)
	recall = recall_score(test_y,pred)
	precision = precision_score(test_y,pred)
	f1 = f1_score(test_y,pred)
	roc = roc_auc_score(test_y,prob)
	fpr, tpr, thresholds = roc_curve(test_y, prob)
	# pyplot.plot([0, 1], [0, 1], linestyle='--')
	# pyplot.plot(fpr, tpr, marker='.')
	# pyplot.show()
	print(name + " Train Accuracy: "+str(train_acc))
	print(name +" Validation Accuracy Mean: "+str(val_acc))
	print(name +" Test Accuracy: "+str(test_acc))
	print(name + " Test Recall: "+str(recall))
	print(name + " Test Precision: "+str(precision))
	print(name + " Test F1 Score: "+str(f1))
	print(name + " Test ROC_AUC Score: "+str(roc))
	print("                                   ")
	
	                            
def main():
	train_x,test_x,train_y,test_y,x,y = load_and_preprocess()
	model_dtree,model_svm,model_rf = models(train_x,train_y,x,y)
	metrics("Dtree",model_dtree,x,y,train_x,test_x,train_y,test_y)
	metrics("SVM",model_svm,x,y,train_x,test_x,train_y,test_y)
	metrics("Random Forest",model_rf,x,y,train_x,test_x,train_y,test_y)

if __name__== "__main__":
  main()
