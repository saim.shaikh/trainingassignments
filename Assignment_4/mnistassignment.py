from mnist import MNIST
import numpy as np
from matplotlib import pyplot
import pickle





#This function calculates the accuracy by comparing the predicted values
#with the actual labels
def accuracy(a2,y):
	a2_index = np.argmax(a2,axis=0)
	y_index = np.argmax(y,axis = 0)
	print(np.sum(a2_index==y_index)/y.shape[1])

#This functions will loaad the saved weights,test data and test the accuracy
#on the test dataset
def test():
	saved_weights = load_weight()
	w1 = saved_weights["w1"]
	b1 = saved_weights["b1"]
	w2 = saved_weights["w2"]
	b2 = saved_weights["b2"]
	mndata = MNIST('Samples')
	x,y = mndata.load_testing()
	y = one_hot(y)
	x = x.T
	y = y.T
	z1 = np.dot(w1.T,x)+b1
	# print(w2.shape)
	a1 = sigmoid(z1)
	z2 = np.dot(w2.T,a1)+b2
	a2 = softmax(z2)
	# lossval = loss(a2,y)
	print("Test Accuracy")
	accuracy(a2,y)

#The softmax activation value is calculated for the 2nd layer values
def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0,keepdims = True)

#Sigmoid activation is calculated for the 1st layer
def sigmoid(x):
	sig = 1/(1+np.exp(-x))
	return sig

#Derivaive of the sigmoid function used in bckpropogation
def sigmoid_der(x):
	sig = x * (1 - x)
	return sig

#Calculates Mean Square Error 
def loss(a2,y):
	return np.average(np.square(np.subtract(a2,y)))

#Derivaive of the softmax function
def softmax_der(smax):
	return smax * (1-smax)

#Used for One Hot Encoding the Labels
def one_hot(y):
	enc_y = []
	for i in y:
		# print(i)
		arr = np.zeros(10,dtype ='uint8')
		arr[i] = 1
		# print(arr)
		enc_y.append(arr)
	enc_y = np.array(enc_y)	
	return  enc_y	

#Loads data from the MniST Dataset 
def load_data():
	mndata = MNIST('Samples')
	x,y = mndata.load_training()
	# test_x,test_y = mndata.load_test()
	x = np.array(x)
	y = np.array(y)
	return x,y

#Saves weight in the form of a pickle file
def save_weight(weight_dict):
	pickle_out = open("weight_dict.pickle","wb")
	pickle.dump(weight_dict, pickle_out)
	pickle_out.close()
	print("Weights Saved")

#Loads the saved weights for testing
def load_weight():
	pickle_in = open("weight_dict.pickle","rb")
	weight_dict = pickle.load(pickle_in)
	return weight_dict

#Validates the data usining the val set cosisting of 10000 images
def validation(x,y,w1,b1,w2,b2):
	z1 = np.dot(w1.T,x)+b1
	# print(w2.shape)
	a1 = sigmoid(z1)
	z2 = np.dot(w2.T,a1)+b2
	a2 = softmax(z2)
	# lossval = loss(a2,y)
	print("Validation Accuracy")
	accuracy(a2,y)

#Preprocesses the data and divides them into traning and validation sets
def preprocess(x,y):
	y = one_hot(y)
	x = x.T
	y = y.T
	train_x = x[:,:50001]
	train_y = y[:,:50001]
	val_x = x[:,50001:]
	val_y = y[:,50001:]
	print(train_x.shape)
	print(val_x.shape)

	return train_x,train_y,val_x,val_y

#Calculates the prediction using the training data
def feedforward(x,y,w1,b1,w2,b2,i):
	z1 = np.dot(w1.T,x)+b1
	# print(w2.shape)
	a1 = sigmoid(z1)
	z2 = np.dot(w2.T,a1)+b2
	a2 = softmax(z2)
	lossval = loss(a2,y)
	print("Epoch: "+str(i))
	print("Accuracy")
	accuracy(a2,y)
	print("Loss")
	print(lossval)
	return z1,a1,z2,a2,lossval

#Calculates the changes required to the weights and bias using Backpropogation
def backpropogation(z1,a1,z2,a2,w2,x,y):
	dz2 = np.subtract(a2,y) * softmax_der(a2)
	dw2 = np.dot(a1,dz2.T) / y.shape[1]
	db2 = np.sum(dz2,axis =1, keepdims =True) / y.shape[1]
	dz1 = np.dot(w2,dz2) * sigmoid_der(a1)
	dw1 = np.dot(x,dz1.T)/ y.shape[1]
	db1 = np.sum(dz1,axis =1, keepdims =True) / y.shape[1]
	return dw1,dw2,db1,db2


def main():
	w1 = np.random.randn(784,300)*0.01
	b1 = np.random.randn(300,1)*0.01
	w2 = np.random.randn(300,10)*0.01
	b2 = np.random.randn(10,1)*0.01
	loss = []
	alpha  = 0.1
	epochs = 1000
	x,y = load_data()
	# y = one_hot(y)
	# x = x.T
	# y = y.T
	# print(x.shape)
	# print(y.shape)
	train_x,train_y,val_x,val_y = preprocess(x,y)

	for i in range(0,epochs):
		z1,a1,z2,a2,lossval = feedforward(train_x,train_y,w1,b1,w2,b2,i)
		loss.append(lossval)
		dw1,dw2,db1,db2 = backpropogation(z1,a1,z2,a2,w2,train_x,train_y)
		w1 = w1 - (alpha * dw1) 
		w2 = w2 - (alpha * dw2)
		b1 = b1 - (alpha * db1)
		b2 = b2 - (alpha * db2)
		validation(val_x,val_y,w1,b1,w2,b2)

	weight_dict = {"w1":w1,"b1":b1,"w2":w2,"b2":b2}	
	save_weight(weight_dict)
	pyplot.plot(loss)
	pyplot.show()
	


	

if __name__== "__main__":
  main()

